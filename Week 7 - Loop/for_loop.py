# for i in range(1, 5):
#     print(i)

# for i in range(1, 9):
#     print(i)  # 1 -> 8

# for i in range(0, 10, 2):  # 0 -> 9
#     print(i)  # 0, 2, 4

# for i in range(10, 0, -1):  # 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
#     print(i)

# For loop in a list
fruits = ['apple', 'banana', 'cherry']

for fruit in fruits[:2]:
    print(fruit)

# For loop in a string
for x in 'banana':
    print(x)
