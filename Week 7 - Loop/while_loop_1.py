month = 0
saved = 0
while month < 24:
    saved += 25  # saved = saved + 25
    month += 1

print(f"In {month} months, you will get: ${saved}")
# print("In {} months, you will get: ${}".format(month, saved))
