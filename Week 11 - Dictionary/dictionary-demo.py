student = {
  'id': 1,
  'name': 'Mr. A',
  'gender': 'M'
}
# print(student)
# print(student['name'])

student['age'] = 20

# print(student)

del student['name']
# student.pop('name')

# print(student)

# student.clear()
# del student
# print(student)

# for key in student:
#   print(student[key])

# for val in student.values():
#   print(val)

# for key, val in student.items():
#   print(f"{key} : {val} ")

print(student['age'])
print(student.get('age'))