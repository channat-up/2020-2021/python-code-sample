def sayHello():
    print("Hello there..")
    print("This is sayHello function")


def greeting(name):
    print(f"Hello {name}")
    sayHello()
    print("Welcome back from market")


# greeting('Lyheng')
# greeting('Socheat')
# greeting('Ratanhu')


def greeting_2(name, message):
    print(f"Welcome {name}")
    print("=================")
    print(message)


# greeting_2('Lenghuy', 'Welcome back to school')
# greeting_2('Lim Chheng', 'Do you want to play mobile legen?')
# greeting_2('Lyheng', 'Do u understand it now?')


def max_number(x, y):
    max = x
    if y > x:
        max = y
    print(f"Max number is: {max}")


# max_number(7, 2)


#              8, 3
def min_number(x, y):
    min = x  # min = 8
    if y < x:  # 3 < 8
        min = y  # min = 3
    print(f"Min number is {min}")


# min_number(8, 3)
