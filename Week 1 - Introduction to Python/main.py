"""

Week 1 - Introduction to Python
27-Feb-2021

"""

# String: We need to use ` ' ` or ` " `
print('Hello world')
print("\n")
print("My name's Sean")
print("Welcome to Python. \n\nI love Python")

print("\tThis is the first paragraph")
print("This is the second paragraph")


