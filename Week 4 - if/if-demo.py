#######################
# IF CONDTION
#######################


# Calculate BMI
weight = 61
height = 1.69

if height > 0:  # TRUE
    bmi = weight / height**2
    print("BMI = %.2f" % bmi)

print("Last line")
# round(100.10129392, 3)


#######################
# AND
# True = 1
# False = 0
# 1 and 1 = 1
# 1 and 0 = 0
# 0 and 1 = 0
# 0 and 0 = 0
#######################

a = 200
b = 33
c = 500
#    1   and  1  = 1
if a > b and c > a:
    print("All conditon are true")


#######################
# OR
# True = 1
# False = 0
# 1 or 1 = 1
# 1 or 0 = 1
# 0 or 1 = 1
# 0 or 0 = 0
#######################
#   0    or  1  = 1
if a > c or c > b:
    print("One of the condtion is True")
