# Naming convention

x = 10
name = "Puthea"
Name = 'Sreypov'

# Snake Case
name = "Ratanhu"
phone_number = "012"
email_address = ""
current_possition = ""

# Camel Case
name = "Huy"
phoneNumber = "011"

# Capitalize
Name = "Pannharith"
PhoneNumber = "010"
