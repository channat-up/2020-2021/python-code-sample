import math

x = 1 # int
y = 3.5 # float

print(type(x))
print(y)
x = 10
print("x = {}".format(x))

# Convert int to float
a = float(x) # 10.0
print("a = {}".format(a))

# x = float(x)
# print("x = {}".format(x))

# 10
# 10.0

# Convert float to int
b = int(y)
print("b = {}".format(b))

c = round(y)
print("c = {}".format(c))


z = x + y
print("z = {}".format(z))

# 2 power 2
print(2**2)

# 3 * 3
print(3*3)

# 3 power 3
print(3**3)

print(math.pi)

print(math.sqrt(84))

# 10 devide by 2
d = 10 / 2
print(d)

x = (9.5*45 - 2.5*3) / (45.5 - 3.5)



r = 50
area = math.pi*r*r
print(area)