import score
from score import sum_score, find_average

math = 89
physic = 100
chemistry = 70
biology = 89

avg = find_average(math, physic, chemistry, biology)
print(avg)

sum = sum_score(math, physic, chemistry, biology)
print(sum)

grade = score.find_grade(avg)
print(grade)