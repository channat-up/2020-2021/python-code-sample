def greeting(name='Someone'):
    print(f'Hi {name}')  # print('Hi {}'.format(name))
    print('Let understand default function')


def add(a, b, c=0, d=0):
    print(a + b + c + d)


# add(2, 4)

# greeting('Sreyneath')

##########################
##  Arbitrary Arugments ##
##########################


# Total = 0
# Total = Total + 4 # 4
# Total = Total + 27 # 31
# Total = Total + 37 # 68
def add_numbers(*numbers):  # numbers = (4, 27, 37)
    total = 0
    for number in numbers:
        total += number
    print(f'Total value is {total}')


# add_numbers(4, 27, 37)
# add_numbers(4, 2)
# add_numbers(4)
# add_numbers()  # 0

##################################
## Arbitrary Keyword Augruments ##
##################################


def show_student_info(**student):
    print(student['name'])


show_student_info(name='Lim Chheng', age=20, gender='F')
