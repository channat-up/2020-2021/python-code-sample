#              x = 30, y = 20
def max_number(x, y):
    max = x  # max = 30
    if y > x:
        max = y
    return max


max_value = max_number(30, 20)

# print(f"Max value is {max_value}")


# |-10| = 10
# |10| = 10
def absolute_value(number):
    if number > 0:
        return number
    else:
        return -number


absolute = absolute_value(10)
# print(f'-> absoute value is: {absolute}')


def is_numbers_has_odd_value(*numbers):
    for num in numbers:
        print(num)  # 10, 28, 12, 3
        if num % 2 != 0:  #  3 % 2 = 1
            return True

    print("----------> SOMETHING")
    return False


# is_odd = is_numbers_has_odd_value(3, 10, 28, 12, 3, 18, 5, 28)

# print(f'-> {is_odd}')


#=========================
def is_numbers_has_odd_value_2(*numbers):
    is_odd = False
    for num in numbers:
        print(num)  # 10, 28, 12, 3
        if num % 2 != 0:  #  3 % 2 = 1
            is_odd = True

    return is_odd


# is_odd = is_numbers_has_odd_value_2(3, 10, 28, 12, 3, 18, 5, 28)
# print(f'---> {is_odd}')


def display(list):
    for l in list:  # ['A3', 'B3', 'C3'],
        for f in l:  # 'A1' 'B1'  None,\n A1, None, \n A3, B3, C3
            print(f, end="\t")
            if f == None:
                return
                # break
        print()


# hotels[1][2] = 'kajsdflkasdjf'
hotels = [
    ['A1', 'B1', None],
    ['A2', None, 'C2'],
    ['A3', 'B3', 'C3'],
]

display(hotels)