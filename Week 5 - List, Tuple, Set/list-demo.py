fruits = ["apple", 'banana', 'cherry', 'orange', 'kiwi', 'mango']

# index
# element

print(fruits[1])  # banana
print(fruits[4])  # kiwi
print(fruits[5])  # mango

print(fruits[-1])  # mango
print(fruits[1:4])  # ['banana', 'cherry', 'orange']

print(fruits[0:3])  # ['apple', 'banana', 'cherry']
# OR
print(fruits[:3])

print(fruits[2:6])  # ['cherry', 'orange', 'kiwi', 'mango']
# or
print(fruits[2:])
