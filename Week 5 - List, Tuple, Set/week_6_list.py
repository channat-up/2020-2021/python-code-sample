fruits = ["apple", 'banana', 'cherry', 'orange', 'kiwi', 'mango']

# print("==> Original", fruits)

# change one item value
# fruits[1] = 'blackcurrent'
# print(fruits)

# Change multiple items
# fruits[2:4] = ['blackcurrent', 'watermelon', 'Jujube']
# print(fruits)

# Append items : append an element
fruits.append('Papaya')
# print(fruits)

# Extend: append list
fruits.extend(['Papaya'])
print(fruits)

# Insert into a specific index
fruits.insert(1, 'Papaya')
# print(fruits)

# fruits = ['apple', 'banana', 'cherry']
# print("=> original", fruits)


# remove by value
# fruits.remove('banana')
# print(fruits)
#           0         1
# fruits = ['apple', 'cherry']

# remove by index
# fruits.pop(2)
# OR
# del fruits[2]
# print(fruits)

# Clear item from list
# fruits.clear()
# print(fruits)

# Delete entire list
# del fruits
# print(fruits)  # error

fruits = ['kiwi', 'orange', "apple", 'cherry', 'mango', 'banana', ]
# print("=> original", fruits)

# Sort
# fruits.sort()
# print(fruits)

# numbers = [30, 100, 10, 50]
# print("=> original", numbers)

# numbers.sort(reverse=True) # reverse is called Argument or parameter
# print(numbers)

# numbers.sort() the same as numbers.sort(reverse=False)

# index      0        1
fruits = ['apple', 'banana']

favorite_fruits = fruits.copy()
# favorite_fruits = fruits

favorite_fruits[1] = 'jujube'

print("=> fruits: ", fruits)
print("=> favorite: ", favorite_fruits)


# Add new element into list to the end
# append() : add as a single element
# extend() :

x = [1, 2, 3]
x1 = [1, 2, 3]
y = [4, 5]

x.append(y)  # [1, 2, 3, [4, 5]]
x1.extend(y)  # [1, 2, 3, 4, 5]

print(x)
print(x1)
