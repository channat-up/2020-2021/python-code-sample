b = [1, 2, 3]
a = [[1, 2, 3], [4, 5, 6]]

# print(a)
# print(a[0])
# print(a[1])
# print(a[0][2])
# print(a[1][1])  # 5,

a[1][2] = 7
# print(a)

hotels = [['Mr. A', 'Mr. B', 'Mr. C'], ['Mr. D', 'Mr. E', 'Mr. F']]
# print(hotels[0])
# print(hotels[1][1])


students = [
    [1, 'Mr. A', 'M'], 
    [2, 'Mr. B', 'M',], 
    [3, 'Ms. C', 'F']
]

# for i in range(len(students)):
#     print(students[i][1])
        

# for stu in students: 
#     print(stu[1])

# for stu in students: 
#     print(stu)


hotels = [
    ['Mr. A', 'Mr. B'],  # Floor: 0
    ['Mr. D', 'Mr. E'],  # Floor: 1
    ['Mr. G', 'Mr. H'],   # Floor: 2,
    ['Mr. J']   # Floor: 3
]

# for floor in hotels:
#     for person in floor:
#         print(person)


# for row in range(len(hotels)): 
#     for col in range(len(hotels[row])):
#         print(hotels[row][col])
